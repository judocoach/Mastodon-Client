use strict;
use warnings;

use Test::Exception;
use Test::More;
use Test::Warnings 'warning';

use Mastodon::Client;

my $client = Mastodon::Client->new(
    client_id     => 'id',
    client_secret => 'secret',
    access_token  => 'token',
);

ok warning { $client->register; },
    'Warns if client already has ID and secret';

done_testing();
